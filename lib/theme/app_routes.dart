import 'package:flutter/material.dart';
import 'package:speechassistive/views/about_us.dart';
import 'package:speechassistive/views/login_screen.dart';
import 'package:speechassistive/views/menu.dart';
import 'package:speechassistive/views/registration_screen.dart';
import 'package:speechassistive/views/settings.dart';

class AppRoutes {
  AppRoutes._();

  static const String authLogin = '/auth-login';
  static const String authRegister = '/auth-register';
  static const String menu = '/menu';
  static const String aboutUs = '/about-us';
  static const String settings = '/settings';

  static Map<String, WidgetBuilder> define() {
    return {
      authLogin: (context) => Login(),
      authRegister: (context) => Register(),
      menu: (context) => MenuScreen(),
      aboutUs: (context) => AboutUs(),
      settings: (context) => AppSetting(),
    };
  }
}