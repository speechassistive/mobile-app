import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:speechassistive/net/firebase.dart';
import 'package:speechassistive/theme/app_routes.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:intl/intl.dart';
import 'package:speechassistive/views/login_screen.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();

}

class _RegisterState extends State<Register> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _firstnameController = TextEditingController();
  TextEditingController _lastnameController = TextEditingController();
  TextEditingController _phonenumberController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _repasswordController = TextEditingController();

  String fname, lname, email, avatar, created_at, phone, _timeString;


  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context);

    void _getTime() {
      final String formattedDateTime = DateFormat('yyyy-MM-dd \n kk:mm:ss').format(DateTime.now()).toString();
      setState(() {
        _timeString = formattedDateTime;
      });
    }

    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());

    final logo = Image.asset(
      "assets/app-logo.png",
      height: mq.size.height / 4,
    );

    final firstnameField = TextFormField(
      controller: _firstnameController,
      keyboardType: TextInputType.name,
      style: TextStyle(
        color: Colors.black,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'First name is empty!';
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "Characters only.",
        labelText: "First Name",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final lastnameField = TextFormField(
      controller: _lastnameController,
      keyboardType: TextInputType.name,
      style: TextStyle(
        color: Colors.black,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Last name is empty!';
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "Characters only.",
        labelText: "Last Name",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final phonenumField = TextFormField(
      controller: _phonenumberController,
      keyboardType: TextInputType.phone,
      maxLength: 11,
      style: TextStyle(
        color: Colors.black,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Phone number is empty!';
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "Numbers only.",
        labelText: "Phone number",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final emailField = TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(
        color: Colors.black,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Email address is empty!';
        }
        if (!value.contains('@')) {
          return 'Not valid email address!';
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "something@example.com",
        labelText: "Email",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final passwordField = TextFormField(
      obscureText: true,
      controller: _passwordController,
      style: TextStyle(
        color: Colors.black,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Password is empty!';
        }

        if (value.length < 6) {
          return 'Password must be 6 characters';
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "Password must be 6 characters.",
        labelText: "Password",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final repasswordField = TextFormField(
      obscureText: true,
      controller: _repasswordController,
      style: TextStyle(
        color: Colors.black,
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Password is empty!';
        }
        return null;
      },
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "Re-type password",
        labelText: "Re-type password",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final fields = Padding(
      padding: EdgeInsets.only(top: 5.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          firstnameField,
          Padding(padding: EdgeInsets.only(top: 10)),
          lastnameField,
          Padding(padding: EdgeInsets.only(top: 10)),
          phonenumField,
          Padding(padding: EdgeInsets.only(top: 10)),
          emailField,
          Padding(padding: EdgeInsets.only(top: 10)),
          passwordField,
          Padding(padding: EdgeInsets.only(top: 10)),
          repasswordField,
        ],
      ),
    );

    final registerButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(25.0),
      color: Colors.blue,
      child: MaterialButton(
        minWidth: mq.size.width / 1.2,
        padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
        child: Text(
          "Register",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        onPressed: () async {
          try {
            if (_formKey.currentState.validate()) {
              await Firebase.initializeApp();

              fname = _firstnameController.text;
              lname = _lastnameController.text;
              email = _emailController.text;
              phone = _phonenumberController.text;
              avatar = null;
              created_at = _timeString;

              final User currentUser = (await FirebaseAuth.instance.createUserWithEmailAndPassword(email: _emailController.text, password: _passwordController.text)).user;
              FirebaseFirestore.instance.collection("users").doc(currentUser.uid).set({
                'user_id': currentUser.uid,
                'created_at': created_at,
                'first_name': fname,
                'last_name': lname,
                'email': email,
                'phone': phone,
                'avatar_url': avatar,
              },
              );

              Navigator.of(context).pushNamed(AppRoutes.authLogin);
            }
          } on FirebaseAuthException catch (e) {
            if (e.code == 'weak-password') {
              print('The password provided is too weak.');
              Flushbar(
                message:  "Password too weak. We suggest to change it.",
                duration:  Duration(seconds: 3),
              )..show(context);
            } else if (e.code == 'email-already-in-use') {
              print('The account already exists for that email.');
              Flushbar(
                message:  "The account already exists for that email. Please use other valid email.",
                duration:  Duration(seconds: 3),
              )..show(context);
            }

            if (_passwordController.text.length < 6) {
              print('The password length must be minimum of 6 characters.');
              Flushbar(
                message:  "The password length must be minimum of 6 characters.",
                duration:  Duration(seconds: 3),
              )..show(context);
            }

            if (_passwordController.text != _repasswordController.text) {
              print('The password does not match. Please check again.');
              Flushbar(
                message:  "The password does not match. Please check again.",
                duration:  Duration(seconds: 3),
              )..show(context);
            }
          } catch (e) {
            print(e);
          }
        },
      ),
    );

    final bottom = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        registerButton,
        Padding(
          padding: EdgeInsets.all(8.0),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Already have an account?",
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(color: Colors.blue),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.of(context).pushNamed(AppRoutes.authLogin);
                /* Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Login()),
                      (Route<dynamic> route) => false,
                ); */
              },
              child: Text(
                "Login",
                style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Colors.blue, decoration: TextDecoration.underline),
              ),
            ),
          ],
        ),
      ],
    );

    return Scaffold(
      extendBody: true,
      appBar: PreferredSize (
       preferredSize: Size.fromHeight(60),
       child: AppBar(
           shape: RoundedRectangleBorder(
               borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))
           ),
           centerTitle: true,
           title: Text('Registration',
               style:
               TextStyle(
                   fontSize: 30.0,
                   color: Colors.white,
                   fontWeight: FontWeight.bold)
           ),
       ),
      ),
      backgroundColor: Colors.white,
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(36, 0, 36, 0),
            child: Container(
              height: mq.size.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  //logo,
                  fields,
                  Padding(
                    padding: EdgeInsets.only(bottom: 150),
                    child: bottom,
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
