import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:speechassistive/theme/app_routes.dart';
import 'package:speechassistive/views/about_us.dart';
import 'package:speechassistive/views/settings.dart';

final usersRef = FirebaseFirestore.instance.collection('users');
final currentUser = FirebaseAuth.instance.currentUser;
String firstname, lastname, email;
String userID = currentUser.uid;


class SideDrawer extends StatefulWidget {
  const SideDrawer({Key key}) : super(key: key);

  @override
  _SideDrawerState createState() => _SideDrawerState();
}

class _SideDrawerState extends State<SideDrawer> {

  @override
  void initState() {
    getUserInfo();
    print("User ID: " + currentUser.uid);
    print("Full name: " + firstname + " " + lastname);
    print("Email " + email);
    super.initState();
  }

  getUserInfo() async {
    DocumentSnapshot docSnap = await FirebaseFirestore.instance.collection('users').doc(userID).get();
    firstname = docSnap['first_name'];
    lastname = docSnap['last_name'];
    email = docSnap['email'];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea( child: Drawer(
        child:
        ListView(
          children: [
            UserAccountsDrawerHeader(
                accountName: Text(
                  firstname + " " + lastname,
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                accountEmail: Text(
                    email,
                    style: TextStyle(
                      fontSize: 16,
                    ),
                ),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: NetworkImage(""),
                ),
            ),
            ListTile(
              title: Text('Home'),
              leading: Icon(Icons.home),
              onTap: () =>
                  Navigator.popAndPushNamed(context, AppRoutes.menu),

            ),
            ListTile(
              title: Text('About Us'),
              leading: Icon(Icons.info),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(_aboutUsAnim());
              }
            ),
            ListTile(
              title: Text('Settings'),
              leading: Icon(Icons.settings),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(_settingsAnim());
                }
            ),
            ListTile(
              title: Text('Log out'),
              leading: Icon(Icons.logout),
              onTap: () => Navigator.pushReplacementNamed(context, AppRoutes.authLogin),
            )
          ],
        ),
      ),
    ),
    );
  }
}

Route _aboutUsAnim() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AboutUs(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

Route _settingsAnim() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => AppSetting(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
