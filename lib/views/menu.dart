import 'package:flutter/material.dart';
import 'package:speechassistive/views/profile_screen.dart';
import 'package:speechassistive/views/side_drawer.dart';
import 'package:speechassistive/views/vocabulary_screen.dart';

import 'practice_screen.dart';
import 'home_screen.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({Key key}) : super(key: key);

  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Welcome')),
      drawer: SideDrawer(),
      body: DefaultTabController(
        length: 4,
        child: Stack(
          children: <Widget>[
            Container(
              height: double.infinity,
              width: double.infinity,
            ),
            Scaffold(
              bottomNavigationBar: Padding(
                padding: EdgeInsets.only(bottom: 0),
                child: TabBar(
                  tabs: <Widget>[
                    Tab(icon: Icon(Icons.home), child: Text("Home")),
                    Tab(icon: Icon(Icons.chat), child: Text("Drill")),
                    Tab(icon: Icon(Icons.record_voice_over), child: Text("Vocabulary")),
                    Tab(icon: Icon(Icons.account_circle), child: Text("Profile")),
                  ],
                  labelColor: Colors.blue,
                  indicator: UnderlineTabIndicator(
                    borderSide: BorderSide(
                      color: Colors. blue, width: 4.0,
                    ),
                    insets: EdgeInsets.only(bottom: 75),
                  ),
                  unselectedLabelColor: Colors.grey,
                ),
              ),

              body: TabBarView(
                children: <Widget>[
                  HomeScreen(),
                  PracticeScreen(),
                  VocabularyScreen(),
                  ProfileScreen(),
                ],
              ),

            ),
          ],
        ),
      ),
    );
  }
}
