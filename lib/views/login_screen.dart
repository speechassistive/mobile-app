import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:speechassistive/theme/app_routes.dart';
import 'package:speechassistive/views/registration_screen.dart';
import 'package:speechassistive/widget/CustomShowDialog.dart';

import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_indicator/progress_indicator.dart';

import 'menu.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _resetKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _emailControllerForgot = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  bool _showLoading = false;

  @override
  Widget build(BuildContext context) {
      final mq = MediaQuery.of(context);

      void _showToastMessage() {
        Fluttertoast.showToast(msg: "Please enter your registered email address.",
        backgroundColor: Colors.black12,
        textColor: Colors.white,
        gravity: ToastGravity.BOTTOM,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 3);
      }

      void showAlertDialog(BuildContext context) {
      showDialog(
          context: context,
          builder: (BuildContext context){
            return CustomAlertDialog(
              content: Container(
                key: _resetKey,
                width: MediaQuery.of(context).size.width / 1.2,
                height: MediaQuery.of(context).size.height / 5,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text("Insert reset email:"),
                    TextFormField(
                      controller: _emailControllerForgot,
                      keyboardType: TextInputType.emailAddress,
                      validator: (val) => val.isEmpty ? "Email address is empty!" : null,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue),
                        ),
                        helperText: "something@example.com",
                        labelText: "Email",
                        labelStyle: TextStyle(
                          color: Colors.black38,
                        ),
                        hintStyle: TextStyle(color: Colors.black12),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.all(15),
                        child: Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(25.0),
                        color: Colors.blue,
                        child: MaterialButton(
                          minWidth: mq.size.width / 2,
                          padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
                          child: Text(
                            'Send reset email',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          onPressed: () async {
                            print('Send email button pressed');
                                try {
                                  if (_emailControllerForgot.text.toString().isEmpty) {
                                    _showToastMessage();
                                  } else {
                                    try {
                                      await Firebase.initializeApp();
                                      FirebaseAuth.instance.sendPasswordResetEmail(
                                          email: _emailControllerForgot.text);
                                      Navigator.of(context).pop();
                                    } catch (e) {

                                    }
                                  }
                                } catch (e) {
                                  print(e);
                                }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        );
      }

    final logo = Image.asset(
      "assets/app-logo.png",
      height: mq.size.height / 4,
    );

    final emailField = TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      style: TextStyle(
        color: Colors.black87,
      ),
      validator: (val) => val.isEmpty ? "Email address is empty!" : null,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue),
        ),
        helperText: "something@example.com",
        labelText: "Email",
        labelStyle: TextStyle(
          color: Colors.black38,
        ),
        hintStyle: TextStyle(color: Colors.black12),
      ),
    );

    final passwordField = Column(
      children: <Widget>[
        TextFormField(
          obscureText: true,
          controller: _passwordController,
          style: TextStyle(
            color: Colors.black87,
          ),
          validator: (val) => val.isEmpty ? "Password is empty!" : null,
          textInputAction: TextInputAction.send,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue),
            ),
            helperText: "Password must be 6 characters.",
            labelText: "Password",
            labelStyle: TextStyle(
              color: Colors.black38,
            ),
            hintStyle: TextStyle(color: Colors.black38),
          ),
        ),
        Padding(
          padding: EdgeInsets.all(2.0),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            MaterialButton(
              child: Text(
                ""
                "Forgot Password",
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: Colors.blue),
              ),
              onPressed: () {
                // TODO: Create forgot password popup.
                showAlertDialog(context);
              },
            )
          ],
        )
      ],
    );

    final fields = Padding(
      padding: EdgeInsets.only(top: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          emailField,
          Padding(padding: EdgeInsets.only(top: 10)),
          passwordField,
        ],
      ),
    );

    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(25.0),
      color: Colors.blue,
      child: MaterialButton(
        minWidth: mq.size.width / 1.2,
        padding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
        child: Text(
          "Login",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: 20.0, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        onPressed: () async {

          try {
            setState(() {
              _showLoading = true;
            });
              if (_formKey.currentState.validate()) {
                await Firebase.initializeApp();
                UserCredential user = (await FirebaseAuth.instance
                    .signInWithEmailAndPassword(
                    email: _emailController.text.trim(),
                    password: _passwordController.text.trim())
                    .whenComplete(() => setState(() {
                        _showLoading = false;
                    })
                )
              );
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => MenuScreen()),
                      (Route<dynamic> route) => false,
                );

            } else {
                setState(() {
                  _showLoading = false;
                });
              Flushbar(
                message:  "No email and password. Please enter your account.",
                duration:  Duration(seconds: 3),
              )..show(context);
            }
          } on FirebaseAuthException catch (e) {
              setState(() {
                _showLoading = false;
              });
              Flushbar(
                message:  "Email or password is incorrect. Please try again.",
                duration:  Duration(seconds: 3),
              )..show(context);

            if (_passwordController.text.length < 5) {
              setState(() {
                _showLoading = false;
              });
              print('The password length must be minimum of 6 characters.');
            }
          } catch (e) {
            print(e);
          }
        },
      ),
    );

    final bottom = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        loginButton,
        Padding(
          padding: EdgeInsets.all(8.0),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              "Not a member?",
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  .copyWith(color: Colors.blue),
            ),
            MaterialButton(
              onPressed: () {
                _emailController.text = "";
                _passwordController.text = "";
                Navigator.of(context).pushNamed(AppRoutes.authRegister);
                //Navigator.pop(context);
                //Navigator.of(context).push(_signupAnim());
                /* Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Register()),
                      (Route<dynamic> route) => false,
                ); */
              },
              child: Text(
                "Sign Up",
                style: Theme.of(context).textTheme.subtitle1.copyWith(
                    color: Colors.blue, decoration: TextDecoration.underline),
              ),
            ),
          ],
        ),
      ],
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Form(
          key: _formKey,
          child: Stack(
            children: <Widget>[
              SingleChildScrollView(
                padding: EdgeInsets.all(36),
                child: Container(
                  height: mq.size.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      logo,
                      fields,
                      Padding(
                        padding: EdgeInsets.only(bottom: 100),
                        child: bottom,
                      ),
                    ],
                  ),
                ),
              ),
              _showLoading ? Container(
                color: Colors.black12,
                child: Center(child: SpinKitDoubleBounce(
                  color: Colors.blue,
                  size: 80.0,
                )
                ),
              ) : Container (

              )
            ],
          )
          ),
        );
      }
    }

