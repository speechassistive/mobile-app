import 'package:flutter/material.dart';
import 'package:speechassistive/theme/app_routes.dart';
import 'package:speechassistive/views/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: 'Speech Assistive',
      routes: AppRoutes.define(),
      home: SplashScreen(),
    );
  }
}