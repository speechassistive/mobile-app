import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';


FirebaseAuth auth = FirebaseAuth.instance;
DocumentSnapshot documentSnapshot;
String uid = auth.currentUser.uid.toString();
String myDocId= 'user.uid';

Future<void> userSetup(String fname, String lname, String email, String avatar, String created_at, String phone) async {

  CollectionReference user = FirebaseFirestore.instance.collection("users");

  user.add({
    'user_id': uid,
    'doc_id': null,
    'created_at': created_at,
    'first_name': fname,
    'last_name': lname,
    'email': email,
    'phone': phone,
    'avatar_url': avatar,
  });

  await FirebaseFirestore.instance
  .collection("users")
  .doc(uid)
  .get()
  .then((value) {
    documentSnapshot = value;
  });

  print(documentSnapshot.id);

  return;

}